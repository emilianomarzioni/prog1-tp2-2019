﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Casino
    {
        public List<Usuario> ListaUsuariosLogueados { get; set; }
        public List<Jugador> ListaJugadores { get; set; }
        public List<Admin> ListaAdministradores { get; set; }
        public Usuario UsuarioActual { get; set; }
        public int MultiplicadorActual { get; set; }

        string PathDatosAdministradores = Directory.GetCurrentDirectory().Replace("\\bin\\Debug", "") + @"\Datos\DatosAdministradores.txt";
        string PathDatosJugadores = Directory.GetCurrentDirectory().Replace("\\bin\\Debug", "") + @"\Datos\DatosJugadores.txt";
        //string PathDatosJugadores = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DatosJugadores.txt");

        //CONSTRUCTOR
        public Casino()
        {
            MultiplicadorActual = 1;

            ListaJugadores = LeerDatosJugadores(PathDatosJugadores);
            ListaAdministradores = LeerDatosAdministradores(PathDatosAdministradores);
            ListaUsuariosLogueados = new List<Usuario>();
            ListaUsuariosLogueados.AddRange(ListaJugadores);
            ListaUsuariosLogueados.AddRange(ListaAdministradores);
        }

        public int Jugar(List<Apuesta> ApuestasActuales) //la lista de apuestas se pasa como parametro desde el form, por eso se permite esta excepcion
        {
            Jugador jugador1 = UsuarioActual as Jugador;
            int cantidadTotalApostada = 0;
            // Verifica que le alcance el saldo para poder jugar
            foreach(Apuesta a in ApuestasActuales)
            {
                cantidadTotalApostada += a.CantidadDeFichas;
            }
            if (jugador1.Saldo >= cantidadTotalApostada)
            {
                int resultadoJuego = Sortear();
                Jugada nuevaJugada = new Jugada(MultiplicadorActual, resultadoJuego, UsuarioActual.Email);
                foreach (Apuesta Ap in ApuestasActuales)
                {
                    nuevaJugada.AgregarApuesta(Ap); //esta hecho de manera tal que NO se pase la lista como parametro a la clase Jugada
                }

                jugador1.AgregarJugada(nuevaJugada); //actualizo las perdidas/ganancias
                jugador1.AgregarMovimiento(nuevaJugada.ObtenerBalance());
                ActualizarListaJugadores(jugador1);
                UsuarioActual = jugador1;
                ActualizarListaUsuarios(UsuarioActual);

                return resultadoJuego;
            }

            return 10;
        }

        private void ActualizarListaJugadores(Jugador jugador1)
        {
            ListaJugadores.RemoveAll(x => x.Email == jugador1.Email);
            ListaJugadores.Add(jugador1);
            Guardar(ListaJugadores, PathDatosJugadores);
        }
        private void ActualizarListaUsuarios(Usuario usuarioActual)
        {
            ListaUsuariosLogueados.RemoveAll(x => x.Email == usuarioActual.Email);
            ListaUsuariosLogueados.Add(usuarioActual);
        }

        public int Sortear()
        {
            Random random = new Random();
            return random.Next(0, 10);
        }
        public bool GuardarJugadorDesdePantallaPrincipal(string email, string contraseña) //ya existe el jugador, solo cambiamos la contraseña
        {

            if (UsuarioActual is Admin)
            {
                // (siempre va a existir por como llegan los datos)
                
                List<Usuario> sinUsuarioSeleccionado = ListaUsuariosLogueados.FindAll(x => x.Email != email);
                Usuario usr = ListaUsuariosLogueados.Find(x => x.Email == email);
                usr.Contraseña = contraseña;
                sinUsuarioSeleccionado.Add(usr);
                ListaUsuariosLogueados = sinUsuarioSeleccionado;

                List<Jugador> sinActualizarJug = ListaJugadores.FindAll(x => x.Email != email);
                Jugador jug = ListaJugadores.Find(x => x.Email == email);
                jug.Contraseña = contraseña;
                sinActualizarJug.Add(jug);
                ListaJugadores = sinActualizarJug;

                Guardar(ListaJugadores, PathDatosJugadores);

                //guardar ya que es un administrador
                return true;
            }
            else
            {
                //error permisos, quien esta intentando guardar no es un administrador
                return false;
            }

        }

        public bool GuardarJugadorDesdePantallaPrincipal(string email, string contraseña, bool nuevoJugador)//nuevo jugador
        {

            if (UsuarioActual is Admin)
            {
                Jugador nuevo = (new Jugador() { Contraseña = contraseña, Email = email, Saldo = 0, Eliminado = false });

                if (ListaUsuariosLogueados.Find(x => x.Email == email) != null) //se esta queriendo guardar un mail que ya existe
                {
                    return false;
                }
                else //si no existe lo guardo
                {
                    ListaUsuariosLogueados.Add(nuevo);
                    ListaJugadores.Add(nuevo);
                    Guardar(ListaJugadores, PathDatosJugadores);
                }
                //guardar ya que es un administrador, de todas formas solo pueden acceder administradores al formulario
                return true;
            }
            else
            {
                //error permisos, quien esta intentando guardar no es un administrador
                return false;
            }

        }
        public bool BuscarUsuario(string email, string contraseña)
        {
            Usuario busquedaUsuario = ListaUsuariosLogueados.Find(x => x.Email == email && x.Contraseña == contraseña);
            if (busquedaUsuario != null)
            {
                UsuarioActual = busquedaUsuario;
                return true;
            }

            return false;
        }

        public void ActualizarSaldo(int montoAActualizar)  //cuando el jugador carga saldo
        {
            if (UsuarioActual is Jugador)
            {
                Jugador jugador = UsuarioActual as Jugador;
                List<Usuario> listaSinUsuario = ListaUsuariosLogueados.FindAll(x => x != UsuarioActual);
                bool carga = true; 
                jugador.AgregarMovimiento(montoAActualizar, carga); //está cargando saldo y no jugando
                UsuarioActual = jugador;
                listaSinUsuario.Add(UsuarioActual);
                ListaUsuariosLogueados = listaSinUsuario;
            }
        }

        public void ActualizarMultiplicador(int v)
        {
            MultiplicadorActual = v;
        }

        public bool IngresarUsuario(string Email, string Contraseña)
        {
            bool resultadoBusquedaUsuario = BuscarUsuario(Email, Contraseña);
            if (resultadoBusquedaUsuario)
                return true;
            return false;
        }

        public bool ActualizarUsuario(Usuario usuarioSeleccionado)
        {
            if (usuarioSeleccionado.Eliminado)
            {
                List<Usuario> ListaAActualizar = ListaUsuariosLogueados.FindAll(x => x.Email != usuarioSeleccionado.Email);
                ListaUsuariosLogueados = ListaAActualizar; 
                List<Jugador> ListaAActualizarJug = ListaJugadores.FindAll(x => x.Email != usuarioSeleccionado.Email);
                Guardar(ListaAActualizarJug, PathDatosJugadores);
                return true;
            }
            return false;
        }

        public List<Jugada> ListaJugadasRealizadas()
        {
            List<Jugada> Lista = new List<Jugada>();
            if (UsuarioActual is Jugador)
            {
                Jugador jug = UsuarioActual as Jugador;
                Lista.AddRange(jug.ListaJugadas);
            }

            if (UsuarioActual is Admin)
            {
                foreach (Jugador jug in ListaJugadores)
                    Lista.AddRange(jug.ListaJugadas);
            }
            return Lista;
        }

        public List<Jugada> ObtenerListaPorFechasJugada(DateTime fechaInicio, DateTime fechaFin)
        {
            List<Jugada> Lista = new List<Jugada>();
            if (UsuarioActual is Jugador)
            {
                Jugador jug = UsuarioActual as Jugador;
                Lista = jug.ListaJugadas.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin).ToList();
            }

            if (UsuarioActual is Admin)
            {
                foreach (Jugador jug in ListaJugadores)
                    Lista.AddRange(jug.ListaJugadas.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin).ToList());
            }
            return Lista;
        }
        public List<Jugada> ObtenerListaPorFechaYJugadorJugada(DateTime fechaInicio, DateTime fechaFin,string jugadorFiltro)
        {
            List<Jugada> Lista = new List<Jugada>();
            
            List <Jugador> jug = ListaJugadores.FindAll(x => x.Email.Contains(jugadorFiltro)); //le agregue contains en vez de igualarlo
            if (jug != null)
            {
                jug.ForEach(jugador=> Lista.AddRange(jugador.ListaJugadas.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin).ToList()));
            }
            else
            {
                List<Jugada> nuevaLista = new List<Jugada>();
                Lista = nuevaLista;
            }

            return Lista;
        }

        public List<Movimiento> ObtenerListaPorFechasMovimiento(DateTime fechaInicio, DateTime fechaFin)
        {
            List<Movimiento> Lista = new List<Movimiento>();
            if (UsuarioActual is Jugador)
            {
                Jugador jug = UsuarioActual as Jugador;
                Lista = jug.ListaMovimientos.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin).ToList();
            }

            if (UsuarioActual is Admin)
            {
                foreach (Jugador jug in ListaJugadores)
                {
                    if (jug.ListaMovimientos != null)
                    {
                        Lista.AddRange(jug.ListaMovimientos.Where(x => x.Fecha >= fechaInicio && x.Fecha <= fechaFin).ToList());
                    }
                }
            }
            return Lista;
        }

        public List<Movimiento> ObtenerListadoMovimientos()
        {
            List<Movimiento> listaMovimientos = new List<Movimiento>();

            if (UsuarioActual is Jugador)
            {
                Jugador jugador = UsuarioActual as Jugador;

                return jugador.ListaMovimientos;
            }

            if (UsuarioActual is Admin)
            {
                Admin administrador = UsuarioActual as Admin;
                
                foreach (Jugador jug in ListaJugadores)
                {
                    if (jug.ListaMovimientos!=null)
                        listaMovimientos.AddRange(jug.ListaMovimientos);
                }

                return listaMovimientos;
            }
            return listaMovimientos;
        }

        //--------------------------------- GUARDAR ------------------------------------------------------------------------------------
        static void Guardar(List<Jugador> Lista, string path)
        {
            using (StreamWriter file = new System.IO.StreamWriter(path, false))
            {
                string JsonContenido = JsonConvert.SerializeObject(Lista);
                file.Write(JsonContenido);
            }
        }

        static void Guardar(List<Admin> Lista, string path)
        {
            using (StreamWriter file = new System.IO.StreamWriter(path, false))
            {
                string JsonContenido = JsonConvert.SerializeObject(Lista);
                file.Write(JsonContenido);
            }
        }

        //--------------------------------- LEER ------------------------------------------------------------------------------------

        static List<Jugador> LeerDatosJugadores(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path);
                List<Jugador> lista = new List<Jugador>();
                return lista;
            }
            else
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string JsonContenido = file.ReadToEnd();
                    List<Jugador> lista = JsonConvert.DeserializeObject<List<Jugador>>(JsonContenido);

                    if (lista == null)
                        lista = new List<Jugador>();

                    return (lista);
                }
            }
        }

        static List<Admin> LeerDatosAdministradores(string path)
        {
            if (!File.Exists(path))
            {
                File.Create(path);
                List<Admin> lista = new List<Admin>();
                return lista;
            }
            else
            {
                using (StreamReader file = new StreamReader(path))
                {
                    string JsonContenido = file.ReadToEnd();
                    List<Admin> lista = JsonConvert.DeserializeObject<List<Admin>>(JsonContenido);

                    if (lista == null)
                        lista = new List<Admin>();

                    return (lista);
                }
            }
        }


    }
}
