﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Apuesta
    {
        public int NumeroApostado { get; set; } 

        public int CantidadDeFichas { get; set; }

        public Apuesta(int nroApostado, int Cantidad)
        {
            NumeroApostado = nroApostado;
            CantidadDeFichas = Cantidad;
        }
    }
}
