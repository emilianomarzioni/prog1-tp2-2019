﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Movimiento
    {
         public Movimiento()
         {
            Fecha = DateTime.Now;
            Monto = 0;
         }

        public Movimiento(int montoMovimiento, string Email)
        {
            this.Monto = montoMovimiento;
            this.Fecha = DateTime.Now;
            UsuarioJugada = Email;
        }

        public DateTime Fecha { get; set; }
        public int Monto { get; set; }
        public string UsuarioJugada { get; set; }

    }
}
