﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    abstract public class Usuario
    {
        public string Contraseña { get; set; }
        public string Email { get; set; }
        public bool Eliminado { get; set; }

        public abstract List<Jugada> VerJugadas();
        
    }
}
