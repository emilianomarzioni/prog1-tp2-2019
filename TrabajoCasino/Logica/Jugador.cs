﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Jugador : Usuario
    {
        public int Saldo { get; set; }
        public List<Movimiento> ListaMovimientos { get; set; }
        public List<Jugada> ListaJugadas { get; set; }
        public int Balance { get; set; }
        public Jugador()
        {
            Eliminado = false;
            List<Movimiento> x = new List<Movimiento>();
            ListaJugadas = new List<Jugada>();
            x.Add(new Movimiento());
            ListaMovimientos = x;
            Balance = 0;
        }

        public void CargarSaldo()
        {
            Saldo = 0;

            foreach (Movimiento mov in ListaMovimientos)
            {
                Saldo += mov.Monto;
            }
        }

        public void AgregarJugada(Jugada jug)
        {
            ListaJugadas.Add(jug);
        }
        public void AgregarMovimiento(int montoMovimiento)
        {
            Movimiento movimiento = new Movimiento(montoMovimiento, Email);
            ListaMovimientos.Add(movimiento);
            Balance += montoMovimiento;
            CargarSaldo();
        }

        public void AgregarMovimiento(int montoMovimiento, bool carga)
        {
            Movimiento movimiento = new Movimiento(montoMovimiento, Email);
            ListaMovimientos.Add(movimiento);
            CargarSaldo();
        }

        public int ObtenerSaldo()
        {
            return Saldo;
        }
        public int ObtenerBalance()
        {
            return Balance;
        }

        public override List<Jugada> VerJugadas()
        {
            return null;
        }
    }
}
