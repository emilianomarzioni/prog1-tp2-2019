﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
   public class Jugada
    {
        public DateTime Fecha { get; set; }
        public int MontoApostado { get; set; }
        public int MontoGanado { get; set; }
        public int Multiplicador { get; set; }
        public int Resultado { get; set; }
        public List<Apuesta> ListaApuestas { get; set; }
        public string UsuarioJugada { get; set; }

        public Jugada(int multiplicador, int resultado, string email)
        {
            UsuarioJugada = email;
            ListaApuestas = new List<Apuesta>();
            MontoGanado = 0;
            Multiplicador = multiplicador;
            Resultado = resultado;
            Fecha = DateTime.Now;
        }

        public void AgregarApuesta(Apuesta apuestaActual)
        {
            ListaApuestas.Add(apuestaActual);
            ComprobarApuesta(apuestaActual);
        }
        public void ComprobarApuesta(Apuesta ApuestaActual)
        {
            if (ApuestaActual.NumeroApostado == Resultado)
            {
                for (int i = 0; i < ApuestaActual.CantidadDeFichas; i++)
                {
                    MontoGanado += (1 * Multiplicador) + 1;  //se suma uno ya que se devuelve la ficha, al hacer el balance se resta las que puso(entre ellas la ganadora)               
                }
            }
            MontoApostado += ApuestaActual.CantidadDeFichas; //Suponiendo que cada ficha vale 1peso, despues hay que agregar cuando las fichas tienen distinto valor
        }
        public int ObtenerBalance()
        {
            return (MontoGanado - MontoApostado);
        }
    }
}
