﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;


namespace TrabajoCasino
{
    public partial class FormIngresar : Form
    {
        public FormIngresar()
        {
            InitializeComponent();
        }

        private void ButtonIngresar_Click(object sender, EventArgs e)
        {
            IngresarUsuario();
        }

        public void IngresarUsuario()
        {
            string Contraseña = textContraseña.Text;
            string Email = textEmail.Text;


            IFormIngresar padre = this.Owner as IFormIngresar;

            if (padre != null)
            {
                if (padre.IngresarUsuario(Email, Contraseña))
                {
                    Close();
                }
                else
                {
                    mensajesLabel.Text = "Usuario incorrecto o contraseña incorrectos. Reingrese los datos.";
                }
            }

            
        }

        private void buttonSalir_Click(object sender, EventArgs e)
        {
            IFormIngresar padre = this.Owner as IFormIngresar;

            if (padre!=null)
            {
                padre.CerrarApp();
                Close();
            }
        }

        private void FormIngresar_Load(object sender, EventArgs e)
        {

        }
    }
}
