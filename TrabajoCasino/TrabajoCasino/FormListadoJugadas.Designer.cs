﻿namespace TrabajoCasino
{
    partial class FormListadoJugadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ListadoJugadas = new System.Windows.Forms.DataGridView();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ganado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apostado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalendarioJugadas = new System.Windows.Forms.MonthCalendar();
            this.btnBuscarFechas = new System.Windows.Forms.Button();
            this.JugadorFiltro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ListadoJugadas)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListadoJugadas
            // 
            this.ListadoJugadas.AllowUserToAddRows = false;
            this.ListadoJugadas.AllowUserToDeleteRows = false;
            this.ListadoJugadas.BackgroundColor = System.Drawing.Color.White;
            this.ListadoJugadas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Goldenrod;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ListadoJugadas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ListadoJugadas.ColumnHeadersHeight = 35;
            this.ListadoJugadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ListadoJugadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Usuario,
            this.Fecha,
            this.Resultado,
            this.Ganado,
            this.Apostado});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ListadoJugadas.DefaultCellStyle = dataGridViewCellStyle2;
            this.ListadoJugadas.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListadoJugadas.EnableHeadersVisualStyles = false;
            this.ListadoJugadas.GridColor = System.Drawing.Color.White;
            this.ListadoJugadas.Location = new System.Drawing.Point(0, 0);
            this.ListadoJugadas.Name = "ListadoJugadas";
            this.ListadoJugadas.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ListadoJugadas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ListadoJugadas.RowHeadersVisible = false;
            this.ListadoJugadas.RowHeadersWidth = 62;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.ListadoJugadas.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.ListadoJugadas.RowTemplate.Height = 35;
            this.ListadoJugadas.Size = new System.Drawing.Size(1218, 712);
            this.ListadoJugadas.TabIndex = 0;
            // 
            // Usuario
            // 
            this.Usuario.DataPropertyName = "UsuarioJugada";
            this.Usuario.HeaderText = "Usuario";
            this.Usuario.MinimumWidth = 8;
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            this.Usuario.Width = 150;
            // 
            // Fecha
            // 
            this.Fecha.DataPropertyName = "Fecha";
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.MinimumWidth = 8;
            this.Fecha.Name = "Fecha";
            this.Fecha.ReadOnly = true;
            this.Fecha.Width = 150;
            // 
            // Resultado
            // 
            this.Resultado.DataPropertyName = "Resultado";
            this.Resultado.HeaderText = "Resultado";
            this.Resultado.MinimumWidth = 8;
            this.Resultado.Name = "Resultado";
            this.Resultado.ReadOnly = true;
            this.Resultado.Width = 150;
            // 
            // Ganado
            // 
            this.Ganado.DataPropertyName = "MontoGanado";
            this.Ganado.HeaderText = "Ganado";
            this.Ganado.MinimumWidth = 8;
            this.Ganado.Name = "Ganado";
            this.Ganado.ReadOnly = true;
            this.Ganado.Width = 150;
            // 
            // Apostado
            // 
            this.Apostado.DataPropertyName = "MontoApostado";
            this.Apostado.HeaderText = "Apostado";
            this.Apostado.MinimumWidth = 8;
            this.Apostado.Name = "Apostado";
            this.Apostado.ReadOnly = true;
            this.Apostado.Width = 150;
            // 
            // CalendarioJugadas
            // 
            this.CalendarioJugadas.Location = new System.Drawing.Point(1230, 23);
            this.CalendarioJugadas.MaxSelectionCount = 365;
            this.CalendarioJugadas.Name = "CalendarioJugadas";
            this.CalendarioJugadas.TabIndex = 1;
            // 
            // btnBuscarFechas
            // 
            this.btnBuscarFechas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.btnBuscarFechas.FlatAppearance.BorderColor = System.Drawing.Color.Goldenrod;
            this.btnBuscarFechas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarFechas.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarFechas.ForeColor = System.Drawing.Color.White;
            this.btnBuscarFechas.Location = new System.Drawing.Point(1314, 468);
            this.btnBuscarFechas.Name = "btnBuscarFechas";
            this.btnBuscarFechas.Size = new System.Drawing.Size(198, 55);
            this.btnBuscarFechas.TabIndex = 2;
            this.btnBuscarFechas.Text = "Buscar";
            this.btnBuscarFechas.UseVisualStyleBackColor = false;
            this.btnBuscarFechas.Click += new System.EventHandler(this.btnBuscarFechas_Click);
            // 
            // JugadorFiltro
            // 
            this.JugadorFiltro.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JugadorFiltro.Location = new System.Drawing.Point(4, 60);
            this.JugadorFiltro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.JugadorFiltro.Name = "JugadorFiltro";
            this.JugadorFiltro.Size = new System.Drawing.Size(310, 31);
            this.JugadorFiltro.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Jugador:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.JugadorFiltro);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1257, 343);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(328, 117);
            this.panel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1252, 658);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(254, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "*Para seleccionar una fecha presione el día,";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1252, 678);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(339, 34);
            this.label3.TabIndex = 7;
            this.label3.Text = "*Para seleccionar un rango de fechas mantenga presionado";
            // 
            // FormListadoJugadas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1587, 712);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CalendarioJugadas);
            this.Controls.Add(this.btnBuscarFechas);
            this.Controls.Add(this.ListadoJugadas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FormListadoJugadas";
            this.ShowIcon = false;
            this.Text = "Jugadas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormListadoJugadas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ListadoJugadas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ListadoJugadas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Resultado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ganado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apostado;
        private System.Windows.Forms.MonthCalendar CalendarioJugadas;
        private System.Windows.Forms.Button btnBuscarFechas;
        private System.Windows.Forms.TextBox JugadorFiltro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}