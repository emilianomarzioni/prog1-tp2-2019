﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace TrabajoCasino
{
    public partial class FormListadoUsuarios : Form, IFormGuardarJugadorClasePrincipal
    {
        public FormListadoUsuarios()
        {
            InitializeComponent();
        }

        public List<Usuario> ObtenerListaUsuarios()
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            return padre.ObtenerListaUsuarios();
        }
        public bool ActualizarUsuario(Usuario user)
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            return padre.ActualizarUsuario(user);
        }
        public Usuario ObtenerUsuarioPorEmail(string email)
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            return padre.ObtenerUsuarioPorEmail(email);
        }
        public void ActualizarListadoUsuarios()
        {
            Listado.AutoGenerateColumns = false;
            Listado.DataSource = ObtenerListaUsuarios();
        }
        public bool GuardarJugador(string Email, string Contraseña)
        {
            IFormGuardarJugadorClasePrincipal padre = this.Owner as IFormGuardarJugadorClasePrincipal;
            padre.GuardarJugador(Email, Contraseña);
            ActualizarListadoUsuarios();
            return true;

        }
        public bool GuardarJugador(string Email, string Contraseña, bool nuevo)
        {
            IFormGuardarJugadorClasePrincipal padre = this.Owner as IFormGuardarJugadorClasePrincipal;
            padre.GuardarJugador(Email, Contraseña, nuevo);
            ActualizarListadoUsuarios();
            return true;

        }

        private void Listado_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var column = Listado.Columns[e.ColumnIndex];
            var row = Listado.Rows[e.RowIndex];

            //En column podemos encontrar el nombre de la columna que definimos en el diseñador.

            if (column.Name == "Eliminar")
            {
                var mensaje = MessageBox.Show("Está seguro que desea eliminar el usuario?", "Eliminar Usuario", MessageBoxButtons.OKCancel);

                if (mensaje == DialogResult.OK)
                {
                    Usuario usuarioSeleccionado = row.DataBoundItem as Usuario;
                    usuarioSeleccionado.Eliminado = true;
                    ActualizarUsuario(usuarioSeleccionado);
                    ActualizarListadoUsuarios();
                }
            }

            if (column.Name == "Modificar")
            {
                Usuario filaSeleccionada = row.DataBoundItem as Usuario;              
                Usuario user = ObtenerUsuarioPorEmail(filaSeleccionada.Email);
                FormModificarUsuario formModificar = new FormModificarUsuario(user);
                formModificar.Owner = this;
                formModificar.ShowDialog();
                
            }
        }

        private void Listado_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FormListadoUsuarios_Load(object sender, EventArgs e)
        {
            ActualizarListadoUsuarios();
        }

        private void NuevoUsuario_Click(object sender, EventArgs e)
        {
             
            FormModificarUsuario formModificar = new FormModificarUsuario();
            formModificar.Owner = this;
            formModificar.ShowDialog();
        }
    }
}
