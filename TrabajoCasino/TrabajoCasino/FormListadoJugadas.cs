﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace TrabajoCasino
{
    public partial class FormListadoJugadas : Form
    {
        public FormListadoJugadas()
        {
            InitializeComponent();
        }

        private void FormListadoJugadas_Load(object sender, EventArgs e)
        {
            ActualizarTabla();
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            Usuario usuarioActual = padre.ObtenerUsuarioActual();
           // Usuario usuarioActual = padre.ObtenerUsuarioActual();
            if(usuarioActual is Admin)
            {
                panel1.Visible = true;
            }
            else
            {
                panel1.Visible = false;
            }
        }

        public void ActualizarTabla()
        {
            ListadoJugadas.AutoGenerateColumns = false;
            ListadoJugadas.DataSource = ObtenerListaJugadas();
        }

        public List<Jugada> ObtenerListaJugadas()
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            return padre.ObtenerListaJugadas();
        }

        private void btnBuscarFechas_Click(object sender, EventArgs e)
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            DateTime fechaInicio = CalendarioJugadas.SelectionStart;
            DateTime fechaFin = CalendarioJugadas.SelectionEnd;
            if (JugadorFiltro.Text == "") { 
            ListadoJugadas.DataSource = padre.ObtenerListaPorFechasJugada(fechaInicio, fechaFin);
            }
            else
            {
                ListadoJugadas.DataSource = padre.ObtenerListaPorFechaYJugadorJugada(fechaInicio,fechaFin,JugadorFiltro.Text);
            }
        }

        
    }
}
