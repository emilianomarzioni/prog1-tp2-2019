﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace TrabajoCasino
{
    public partial class FormModificarUsuario : Form
    {
        private Usuario UsuarioSeleccionado;
        public FormModificarUsuario()
        {
            InitializeComponent();
        }
        
        public FormModificarUsuario(Usuario usuarioSeleccionado)
        {
            InitializeComponent();
            UsuarioSeleccionado = usuarioSeleccionado;
            EmailBox.Text = UsuarioSeleccionado.Email;
            ContraseñaBox.Text = UsuarioSeleccionado.Contraseña;
            EmailBox.Enabled = false;
        }

        private void FormModificarUsuario_Load(object sender, EventArgs e)
        {
            
        }

        private void ModificarUsuario_Click(object sender, EventArgs e)
        {
            if (EmailBox.Enabled==false) //simplemente modifica
            {
                IFormGuardarJugadorClasePrincipal padre = this.Owner as IFormGuardarJugadorClasePrincipal;

                padre.GuardarJugador(EmailBox.Text, ContraseñaBox.Text);
                this.Close();
            }

            if (EmailBox.Enabled==true) //guarda uno nuevo
            {
                IFormGuardarJugadorClasePrincipal padre = this.Owner as IFormGuardarJugadorClasePrincipal;

                bool nuevo = true;
                padre.GuardarJugador(EmailBox.Text, ContraseñaBox.Text, nuevo);
                this.Close();
            }
            
        }
    }
}
