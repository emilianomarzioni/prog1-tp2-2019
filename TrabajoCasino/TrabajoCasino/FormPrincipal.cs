﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrabajoCasino
{
    public partial class FormPrincipal : Form, IFormGuardarJugadorClasePrincipal, IFormGrillaJugadores, IFormIngresar
    {
        //-------------------------------------- FORMULARIO -------------------------------------- 
        public Casino ClasePrincipal { get; set; }
        public List<Apuesta> BotonesApuesta { get; set; }
        public bool Cargada { get; set; }
        public bool Cerrar { get; set; }
        public Button BotonActual { get; set; }
        public FormPrincipal()
        {
            InitializeComponent();
            ClasePrincipal = new Casino();
            BotonesApuesta = new List<Apuesta>();
            FormIngresar FormIngresarUsuario = new FormIngresar();
            FormIngresarUsuario.Owner = this; // Es necesario poner el padre o como es directo no hace falta??
            FormIngresarUsuario.ShowDialog();
            Cargada = false;
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            CargarPagina();
            ReiniciarTabla();
            Cargada = true;
        }

        //VISTA CONDICIONAL SEGUN USUARIO
        public void CargarPagina()
        {
            if (Cerrar == true)
                Close();

            if (ClasePrincipal.UsuarioActual is Admin)
            {
                listadoDeDatosDeUsuariosToolStripMenuItem.Visible = true;
                ListadoMovimientosStrip.Visible = true;
                panelJuego.Visible = false;
                panel2.Visible = false;
                agregarUsuarioToolStripMenuItem.Visible = true;
                ListadoMovimientosStrip.Visible = false;
                lblBienvenido.Text = "Bienvenido " + ClasePrincipal.UsuarioActual.Email + "!";
            }
            else if (ClasePrincipal.UsuarioActual is Jugador)
            {
                agregarUsuarioToolStripMenuItem.Visible = false;
                Jugador jugador = ClasePrincipal.UsuarioActual as Jugador;
                listadoDeDatosDeUsuariosToolStripMenuItem.Visible = false;
                ListadoMovimientosStrip.Visible = true;
                panelJuego.Visible = true;
                panel2.Visible = true;
                ListadoMovimientosStrip.Visible = true;

                lblBalance.Text = jugador.ObtenerBalance().ToString();
                SaldoActualLabel.Text = jugador.ObtenerSaldo().ToString();
                lblBienvenido.Text = "Bienvenido " + ClasePrincipal.UsuarioActual.Email + "!";
                BotonActual = Cerobutton;
                ReiniciarTabla();
            }
            
        }
        private void listadoDeDatosDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e) //VENTANA DE LISTA DE INFORMACION DE USUARIOS
        {
            FormListadoUsuarios ListaDatosUsuarios = new FormListadoUsuarios();
            ListaDatosUsuarios.Owner = this; // Es necesario poner el padre o como es directo no hace falta??
            ListaDatosUsuarios.ShowDialog();
        }

        private void agregarUsuarioToolStripMenuItem_Click(object sender, EventArgs e) //VENTANA DE USUARIOS A GUARDAR
        {
            FormModificarUsuario formModificar = new FormModificarUsuario();
            formModificar.Owner = this;
            formModificar.ShowDialog();
        }

        //Cargar Saldo a Jugador
        private void ButtonCargar_Click(object sender, EventArgs e)
        {

            int x;
            int.TryParse(textCargaSaldo.Text, out x); // x es cero si se ingreso texto o si no se puede convertir a nro

            if (x > 0)
            {
                ActualizarSaldo(int.Parse(textCargaSaldo.Text));
                textCargaSaldo.Clear();
            }
            else
            {
                var mensaje = MessageBox.Show("Ingrese solo numeros positivos.", "Importante!", MessageBoxButtons.OK);
                if (mensaje == DialogResult.OK)
                {
                    ReiniciarTabla();
                    BotonesApuesta.Clear();
                    textCargaSaldo.Clear();
                }
            }

        }

        //JUGAR!
        private void SortearButton_Click(object sender, EventArgs e)
        {
            ClasePrincipal.ActualizarMultiplicador(int.Parse(numericUpDown1.Value.ToString()));
            int NroGanador = ClasePrincipal.Jugar(BotonesApuesta); //pasar la lista de apuestas actuales
            if (NroGanador < 10)
            {
                LabelNroGanador.Text = NroGanador.ToString();
                CambiarColorNroGanador(NroGanador);

                Jugador jugador = ClasePrincipal.UsuarioActual as Jugador;
                SaldoActualLabel.Text = jugador.ObtenerSaldo().ToString();
                lblBalance.Text = jugador.ObtenerBalance().ToString();


                var mensaje = MessageBox.Show($"El numero ganador es {NroGanador}", "Resultado", MessageBoxButtons.OK);

                if (mensaje == DialogResult.OK)
                {
                    ReiniciarTabla();
                    BotonesApuesta.Clear();
                }
            }
            else
            {
                var mensaje = MessageBox.Show($"No tiene suficiente saldo", "Saldo Insuficiente", MessageBoxButtons.OK);

                if (mensaje == DialogResult.OK)
                {
                    ReiniciarTabla();
                    BotonesApuesta.Clear();
                }
            }
        }

        private void ListadoMovimientosStrip_Click(object sender, EventArgs e)
        {
            FormListadoMovimientos ListaMovimientos = new FormListadoMovimientos();
            ListaMovimientos.Owner = this;
            ListaMovimientos.ShowDialog();
        }

        private void jugadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormListadoJugadas ListaJugadas = new FormListadoJugadas();
            ListaJugadas.Owner = this;
            ListaJugadas.ShowDialog();
        }

        
        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            ClasePrincipal = new Casino();
            BotonesApuesta = new List<Apuesta>();
            FormIngresar FormIngresarUsuario = new FormIngresar();
            FormIngresarUsuario.Owner = this; // Es necesario poner el padre o como es directo no hace falta??
            FormIngresarUsuario.ShowDialog();

            if (Cerrar==true)
                Close();
            
        }

        // ------------------------------- METODOS ------------------------------------------------------------------

        public bool GuardarJugador(string Email, string Contraseña)
        {
            //ObtenerListaUsuarios();
            return ClasePrincipal.GuardarJugadorDesdePantallaPrincipal(Email,Contraseña);//modifica un jugador existente
        }

        public bool GuardarJugador(string Email, string Contraseña, bool nuevo)  //nuevo jugador
        {
            //ObtenerListaUsuarios();
            return ClasePrincipal.GuardarJugadorDesdePantallaPrincipal(Email, Contraseña, nuevo);
        }
        public void ActualizarSaldo(int montoAActualizar)
        {
            ClasePrincipal.ActualizarSaldo(montoAActualizar);
            Jugador jugador = ClasePrincipal.UsuarioActual as Jugador;
            SaldoActualLabel.Text = jugador.ObtenerSaldo().ToString();
        }
        public List<Usuario> ObtenerListaUsuarios()
        {
            return ClasePrincipal.ListaUsuariosLogueados.FindAll(x=>x.Eliminado != true && x is Jugador);
        }
        public List<Movimiento> ObtenerListaMovimientos()
        {
             return ClasePrincipal.ObtenerListadoMovimientos();
        }
        public List<Jugada> ObtenerListaJugadas()
        {
            return ClasePrincipal.ListaJugadasRealizadas();
        }

        public List<Jugada> ObtenerListaPorFechasJugada(DateTime fechaInicio, DateTime fechaFin)
        {
            return ClasePrincipal.ObtenerListaPorFechasJugada(fechaInicio, fechaFin);
        }
        public List<Jugada> ObtenerListaPorFechaYJugadorJugada(DateTime fechaInicio, DateTime fechaFin,string jugador)
        {
            return ClasePrincipal.ObtenerListaPorFechaYJugadorJugada(fechaInicio, fechaFin, jugador);
        }
        public List<Movimiento> ObtenerListaPorFechasMovimiento(DateTime fechaInicio, DateTime fechaFin)
        {
            return ClasePrincipal.ObtenerListaPorFechasMovimiento(fechaInicio, fechaFin);
        }
        public Usuario ObtenerUsuarioPorEmail(string email)
        {
            return ClasePrincipal.ListaUsuariosLogueados.Find(x => x.Email == email);
        }
        public bool ActualizarUsuario(Usuario usuarioSeleccionado)
        {
            return ClasePrincipal.ActualizarUsuario(usuarioSeleccionado);
        }
        public Usuario ObtenerUsuarioActual()
        {
            return ClasePrincipal.UsuarioActual;
        }

        public bool IngresarUsuario(string Email, string Contraseña)
        {
            bool respuesta= ClasePrincipal.IngresarUsuario(Email, Contraseña); ;
            if (Cargada == true)
            {
                CargarPagina();
            }
            return respuesta;
        }

        public void CerrarApp()
        {
            Cerrar = true;
        }
        private void ReiniciarTabla()
        {
            Cerobutton.BackColor = Color.Gray;
            Unobutton.BackColor = Color.Gray;
            Dosbutton.BackColor = Color.Gray;
            Tresbutton.BackColor = Color.Gray;
            Cuatrobutton.BackColor = Color.Gray;
            Cincobutton.BackColor = Color.Gray;
            Seisbutton.BackColor = Color.Gray;
            Sietebutton.BackColor = Color.Gray;
            Ochobutton.BackColor = Color.Gray;
            Nuevebutton.BackColor = Color.Gray;
            TxtPtos0.Text = "0";
            TxtPtos1.Text = "0";
            TxtPtos2.Text = "0";
            TxtPtos3.Text = "0";
            TxtPtos4.Text = "0";
            TxtPtos5.Text = "0";
            TxtPtos6.Text = "0";
            TxtPtos7.Text = "0";
            TxtPtos8.Text = "0";
            TxtPtos9.Text = "0";
        }

        private void NumeroApostado (object sender, EventArgs e)
        {
            
            //if (btn.Button == System.Windows.Forms.MouseButtons.Right)
            //{
            //    btnRestar_Click();
            //}
            //else
            //{
                Button clickedButton = sender as Button;
                BotonActual = clickedButton;

                if (clickedButton == null) // solo para estar en el lado seguro
                    return;

                if (BotonesApuesta.Find(x => x.NumeroApostado == int.Parse(clickedButton.Text)) == null) // si el boton no se apostó
                {
                    Apuesta ApuestaNueva = new Apuesta(int.Parse(clickedButton.Text), 1);
                    BotonesApuesta.Add(ApuestaNueva); //agrego el boton a la lista de apuestas
                    clickedButton.BackColor = Color.Green;
                    CantidadFichas(clickedButton.Text);
                }
                else // si el boton se apostó
                {
                    BotonesApuesta.Find(x => x.NumeroApostado == int.Parse(clickedButton.Text)).CantidadDeFichas++;
                    CantidadFichas(clickedButton.Text);
                }
            //}
        }

        public void CambiarColorNroGanador(int nro)
        {
            switch (nro)
            {
                case 0:
                    Cerobutton.BackColor = Color.Red;
                    break;

                case 1:
                    Unobutton.BackColor = Color.Red;
                    break;

                case 2:
                    Dosbutton.BackColor = Color.Red;
                    break;

                case 3:
                    Tresbutton.BackColor = Color.Red;
                    break;

                case 4:
                    Cuatrobutton.BackColor = Color.Red;
                    break;

                case 5:
                    Cincobutton.BackColor = Color.Red;
                    break;

                case 6:
                    Seisbutton.BackColor = Color.Red;
                    break;

                case 7:
                    Sietebutton.BackColor = Color.Red;
                    break;

                case 8:
                    Ochobutton.BackColor = Color.Red;
                    break;

                case 9:
                    Nuevebutton.BackColor = Color.Red;
                    break;


                default:
                    break;
            }
        }

        public void CantidadFichas(string nro)
        {
            switch (int.Parse(nro))
            {
                case 0:
                    TxtPtos0.Text = (int.Parse(TxtPtos0.Text)+1).ToString();
                    break;

                case 1:
                    TxtPtos1.Text = (int.Parse(TxtPtos1.Text) + 1).ToString();
                    break;

                case 2:
                    TxtPtos2.Text = (int.Parse(TxtPtos2.Text) + 1).ToString();
                    break;

                case 3:
                    TxtPtos3.Text = (int.Parse(TxtPtos3.Text) + 1).ToString();
                    break;

                case 4:
                    TxtPtos4.Text = (int.Parse(TxtPtos4.Text) + 1).ToString();
                    break;

                case 5:
                    TxtPtos5.Text = (int.Parse(TxtPtos5.Text) + 1).ToString();
                    break;

                case 6:
                    TxtPtos6.Text = (int.Parse(TxtPtos6.Text) + 1).ToString();
                    break;

                case 7:
                    TxtPtos7.Text = (int.Parse(TxtPtos7.Text) + 1).ToString();
                    break;

                case 8:
                    TxtPtos8.Text = (int.Parse(TxtPtos8.Text) + 1).ToString();
                    break;

                case 9:
                    TxtPtos9.Text = (int.Parse(TxtPtos9.Text) + 1).ToString();
                    break;


                default:
                    break;
            }
        }

        public void CantidadFichasRestar(string nro)
        {
            switch (int.Parse(nro))
            {
                case 0:
                    TxtPtos0.Text = (int.Parse(TxtPtos0.Text) - 1).ToString();
                    break;

                case 1:
                    TxtPtos1.Text = (int.Parse(TxtPtos1.Text) - 1).ToString();
                    break;

                case 2:
                    TxtPtos2.Text = (int.Parse(TxtPtos2.Text) - 1).ToString();
                    break;

                case 3:
                    TxtPtos3.Text = (int.Parse(TxtPtos3.Text) - 1).ToString();
                    break;

                case 4:
                    TxtPtos4.Text = (int.Parse(TxtPtos4.Text) - 1).ToString();
                    break;

                case 5:
                    TxtPtos5.Text = (int.Parse(TxtPtos5.Text) - 1).ToString();
                    break;

                case 6:
                    TxtPtos6.Text = (int.Parse(TxtPtos6.Text) - 1).ToString();
                    break;

                case 7:
                    TxtPtos7.Text = (int.Parse(TxtPtos7.Text) - 1).ToString();
                    break;

                case 8:
                    TxtPtos8.Text = (int.Parse(TxtPtos8.Text) - 1).ToString();
                    break;

                case 9:
                    TxtPtos9.Text = (int.Parse(TxtPtos9.Text) - 1).ToString();
                    break;


                default:
                    break;
            }
        }

        private void btnRestar_Click(object sender, EventArgs e)
        {
            Button clickedButton = BotonActual;

            if (BotonesApuesta.Find(x => x.NumeroApostado == int.Parse(clickedButton.Text)) != null) //ya se apostó
            {
                Apuesta boton = BotonesApuesta.Find(x => x.NumeroApostado == int.Parse(clickedButton.Text));

                if ((boton.CantidadDeFichas-1) == 0) //si llego a cero fichas
                {
                    clickedButton.BackColor = Color.Gray;
                    CantidadFichasRestar(clickedButton.Text);
                    BotonesApuesta.Remove(boton);
                    BotonActual = Cerobutton;
                }
                else //si le quedan fichas todavia
                {
                    BotonesApuesta.Remove(boton);
                    boton.CantidadDeFichas--;
                    BotonesApuesta.Add(boton);
                    CantidadFichasRestar(clickedButton.Text);
                }
                
                
            }
            else
            {
                MessageBox.Show("No ha apostado a este número aún, no puede restarle a cero", "Error");
            }
        }

    }
}
