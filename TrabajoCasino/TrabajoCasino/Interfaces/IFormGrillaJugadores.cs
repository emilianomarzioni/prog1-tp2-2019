﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace TrabajoCasino
{
    interface IFormGrillaJugadores
    {
        List<Usuario> ObtenerListaUsuarios();

        List<Movimiento> ObtenerListaMovimientos();

        Usuario ObtenerUsuarioPorEmail(string email);
        bool ActualizarUsuario(Usuario u);

        List<Jugada> ObtenerListaJugadas();
        Usuario ObtenerUsuarioActual();
        List<Jugada> ObtenerListaPorFechasJugada(DateTime fechaInicio, DateTime fechaFin);
        List<Jugada> ObtenerListaPorFechaYJugadorJugada(DateTime fechaInicio, DateTime fechaFin, string Jugador);
        List<Movimiento> ObtenerListaPorFechasMovimiento(DateTime fechaInicio, DateTime fechaFin);
    }
}
