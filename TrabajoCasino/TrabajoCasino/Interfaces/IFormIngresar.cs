﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabajoCasino
{
    public interface IFormIngresar
    {
        bool IngresarUsuario(string Email, string Contraseña);

        void CerrarApp();
    }
}
