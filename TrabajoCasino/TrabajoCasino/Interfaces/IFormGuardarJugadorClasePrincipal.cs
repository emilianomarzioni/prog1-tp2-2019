﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabajoCasino
{
    public interface IFormGuardarJugadorClasePrincipal
    {
        bool GuardarJugador(string Email, string Contraseña);
        bool GuardarJugador(string Email, string Contraseña, bool nuevo);
    }
}
