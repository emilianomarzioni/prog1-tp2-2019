﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace TrabajoCasino
{
    public partial class FormListadoMovimientos : Form
    {
        public FormListadoMovimientos()
        {
            InitializeComponent();
        }
        private void FormListadoMovimientos_Load(object sender, EventArgs e)
        {
            ActualizarListadoMovimientos();
        }
        public void ActualizarListadoMovimientos()
        {
            Listado.AutoGenerateColumns = false;
            Listado.DataSource = ObtenerListaMovimientos();
        }
        public List<Movimiento> ObtenerListaMovimientos()
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            return padre.ObtenerListaMovimientos();
        }

        private void btnBuscarFechas_Click(object sender, EventArgs e)
        {
            IFormGrillaJugadores padre = this.Owner as IFormGrillaJugadores;
            DateTime fechaInicio = CalendarioMovimientos.SelectionStart;
            DateTime fechaFin = CalendarioMovimientos.SelectionEnd;
            Listado.DataSource = padre.ObtenerListaPorFechasMovimiento(fechaInicio, fechaFin);
        }
    }
}
