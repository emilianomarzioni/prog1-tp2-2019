﻿namespace TrabajoCasino
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.listadoDeDatosDeUsuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jugadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListadoMovimientosStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.Cerobutton = new System.Windows.Forms.Button();
            this.Unobutton = new System.Windows.Forms.Button();
            this.Dosbutton = new System.Windows.Forms.Button();
            this.Tresbutton = new System.Windows.Forms.Button();
            this.Cuatrobutton = new System.Windows.Forms.Button();
            this.Cincobutton = new System.Windows.Forms.Button();
            this.Seisbutton = new System.Windows.Forms.Button();
            this.Sietebutton = new System.Windows.Forms.Button();
            this.Ochobutton = new System.Windows.Forms.Button();
            this.Nuevebutton = new System.Windows.Forms.Button();
            this.SortearButton = new System.Windows.Forms.Button();
            this.panelJuego = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelNroGanador = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.btnRestar = new System.Windows.Forms.Button();
            this.TxtPtos3 = new System.Windows.Forms.Label();
            this.TxtPtos6 = new System.Windows.Forms.Label();
            this.TxtPtos9 = new System.Windows.Forms.Label();
            this.TxtPtos8 = new System.Windows.Forms.Label();
            this.TxtPtos5 = new System.Windows.Forms.Label();
            this.TxtPtos2 = new System.Windows.Forms.Label();
            this.TxtPtos0 = new System.Windows.Forms.Label();
            this.TxtPtos7 = new System.Windows.Forms.Label();
            this.TxtPtos4 = new System.Windows.Forms.Label();
            this.TxtPtos1 = new System.Windows.Forms.Label();
            this.LabelMultiplicador = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.SaldoActualLabel = new System.Windows.Forms.Label();
            this.buttonCargar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textCargaSaldo = new System.Windows.Forms.TextBox();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.lblBienvenido = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip2.SuspendLayout();
            this.panelJuego.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.AutoSize = false;
            this.menuStrip2.BackColor = System.Drawing.Color.Goldenrod;
            this.menuStrip2.Font = new System.Drawing.Font("Bahnschrift SemiBold", 12F);
            this.menuStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listadoDeDatosDeUsuariosToolStripMenuItem,
            this.agregarUsuarioToolStripMenuItem,
            this.listadosToolStripMenuItem});
            this.menuStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(0);
            this.menuStrip2.Size = new System.Drawing.Size(1111, 55);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // listadoDeDatosDeUsuariosToolStripMenuItem
            // 
            this.listadoDeDatosDeUsuariosToolStripMenuItem.Name = "listadoDeDatosDeUsuariosToolStripMenuItem";
            this.listadoDeDatosDeUsuariosToolStripMenuItem.Size = new System.Drawing.Size(228, 55);
            this.listadoDeDatosDeUsuariosToolStripMenuItem.Text = "Listado de Datos de Usuarios";
            this.listadoDeDatosDeUsuariosToolStripMenuItem.Click += new System.EventHandler(this.listadoDeDatosDeUsuariosToolStripMenuItem_Click);
            // 
            // agregarUsuarioToolStripMenuItem
            // 
            this.agregarUsuarioToolStripMenuItem.Name = "agregarUsuarioToolStripMenuItem";
            this.agregarUsuarioToolStripMenuItem.Size = new System.Drawing.Size(140, 55);
            this.agregarUsuarioToolStripMenuItem.Text = "Agregar Usuario";
            this.agregarUsuarioToolStripMenuItem.Click += new System.EventHandler(this.agregarUsuarioToolStripMenuItem_Click);
            // 
            // listadosToolStripMenuItem
            // 
            this.listadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jugadasToolStripMenuItem,
            this.ListadoMovimientosStrip});
            this.listadosToolStripMenuItem.Name = "listadosToolStripMenuItem";
            this.listadosToolStripMenuItem.Size = new System.Drawing.Size(82, 55);
            this.listadosToolStripMenuItem.Text = "Listados";
            // 
            // jugadasToolStripMenuItem
            // 
            this.jugadasToolStripMenuItem.Name = "jugadasToolStripMenuItem";
            this.jugadasToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.jugadasToolStripMenuItem.Text = "Jugadas";
            this.jugadasToolStripMenuItem.Click += new System.EventHandler(this.jugadasToolStripMenuItem_Click);
            // 
            // ListadoMovimientosStrip
            // 
            this.ListadoMovimientosStrip.Name = "ListadoMovimientosStrip";
            this.ListadoMovimientosStrip.Size = new System.Drawing.Size(180, 24);
            this.ListadoMovimientosStrip.Text = "Movimientos";
            this.ListadoMovimientosStrip.Click += new System.EventHandler(this.ListadoMovimientosStrip_Click);
            // 
            // Cerobutton
            // 
            this.Cerobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Cerobutton.FlatAppearance.BorderSize = 0;
            this.Cerobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cerobutton.Font = new System.Drawing.Font("Bahnschrift", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cerobutton.ForeColor = System.Drawing.Color.Black;
            this.Cerobutton.Location = new System.Drawing.Point(58, 280);
            this.Cerobutton.Name = "Cerobutton";
            this.Cerobutton.Size = new System.Drawing.Size(67, 58);
            this.Cerobutton.TabIndex = 2;
            this.Cerobutton.Text = "0";
            this.Cerobutton.UseVisualStyleBackColor = false;
            this.Cerobutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Unobutton
            // 
            this.Unobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Unobutton.FlatAppearance.BorderSize = 0;
            this.Unobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Unobutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Unobutton.ForeColor = System.Drawing.Color.Black;
            this.Unobutton.Location = new System.Drawing.Point(130, 219);
            this.Unobutton.Name = "Unobutton";
            this.Unobutton.Size = new System.Drawing.Size(67, 58);
            this.Unobutton.TabIndex = 3;
            this.Unobutton.Text = "1";
            this.Unobutton.UseVisualStyleBackColor = false;
            this.Unobutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Dosbutton
            // 
            this.Dosbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Dosbutton.FlatAppearance.BorderSize = 0;
            this.Dosbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Dosbutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Dosbutton.Location = new System.Drawing.Point(130, 280);
            this.Dosbutton.Name = "Dosbutton";
            this.Dosbutton.Size = new System.Drawing.Size(67, 58);
            this.Dosbutton.TabIndex = 4;
            this.Dosbutton.Text = "2";
            this.Dosbutton.UseVisualStyleBackColor = false;
            this.Dosbutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Tresbutton
            // 
            this.Tresbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Tresbutton.FlatAppearance.BorderSize = 0;
            this.Tresbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tresbutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Tresbutton.Location = new System.Drawing.Point(130, 341);
            this.Tresbutton.Name = "Tresbutton";
            this.Tresbutton.Size = new System.Drawing.Size(67, 58);
            this.Tresbutton.TabIndex = 5;
            this.Tresbutton.Text = "3";
            this.Tresbutton.UseVisualStyleBackColor = false;
            this.Tresbutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Cuatrobutton
            // 
            this.Cuatrobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Cuatrobutton.FlatAppearance.BorderSize = 0;
            this.Cuatrobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cuatrobutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Cuatrobutton.Location = new System.Drawing.Point(204, 219);
            this.Cuatrobutton.Name = "Cuatrobutton";
            this.Cuatrobutton.Size = new System.Drawing.Size(67, 58);
            this.Cuatrobutton.TabIndex = 6;
            this.Cuatrobutton.Text = "4";
            this.Cuatrobutton.UseVisualStyleBackColor = false;
            this.Cuatrobutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Cincobutton
            // 
            this.Cincobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Cincobutton.FlatAppearance.BorderSize = 0;
            this.Cincobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cincobutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Cincobutton.Location = new System.Drawing.Point(204, 280);
            this.Cincobutton.Name = "Cincobutton";
            this.Cincobutton.Size = new System.Drawing.Size(67, 58);
            this.Cincobutton.TabIndex = 7;
            this.Cincobutton.Text = "5";
            this.Cincobutton.UseVisualStyleBackColor = false;
            this.Cincobutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Seisbutton
            // 
            this.Seisbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Seisbutton.FlatAppearance.BorderSize = 0;
            this.Seisbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Seisbutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Seisbutton.Location = new System.Drawing.Point(204, 341);
            this.Seisbutton.Name = "Seisbutton";
            this.Seisbutton.Size = new System.Drawing.Size(67, 58);
            this.Seisbutton.TabIndex = 8;
            this.Seisbutton.Text = "6";
            this.Seisbutton.UseVisualStyleBackColor = false;
            this.Seisbutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Sietebutton
            // 
            this.Sietebutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Sietebutton.FlatAppearance.BorderSize = 0;
            this.Sietebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Sietebutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Sietebutton.Location = new System.Drawing.Point(276, 219);
            this.Sietebutton.Name = "Sietebutton";
            this.Sietebutton.Size = new System.Drawing.Size(67, 58);
            this.Sietebutton.TabIndex = 9;
            this.Sietebutton.Text = "7";
            this.Sietebutton.UseVisualStyleBackColor = false;
            this.Sietebutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Ochobutton
            // 
            this.Ochobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Ochobutton.FlatAppearance.BorderSize = 0;
            this.Ochobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ochobutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Ochobutton.Location = new System.Drawing.Point(276, 280);
            this.Ochobutton.Name = "Ochobutton";
            this.Ochobutton.Size = new System.Drawing.Size(67, 58);
            this.Ochobutton.TabIndex = 10;
            this.Ochobutton.Text = "8";
            this.Ochobutton.UseVisualStyleBackColor = false;
            this.Ochobutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // Nuevebutton
            // 
            this.Nuevebutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.Nuevebutton.FlatAppearance.BorderSize = 0;
            this.Nuevebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Nuevebutton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.Nuevebutton.Location = new System.Drawing.Point(276, 341);
            this.Nuevebutton.Name = "Nuevebutton";
            this.Nuevebutton.Size = new System.Drawing.Size(67, 58);
            this.Nuevebutton.TabIndex = 11;
            this.Nuevebutton.Text = "9";
            this.Nuevebutton.UseVisualStyleBackColor = false;
            this.Nuevebutton.Click += new System.EventHandler(this.NumeroApostado);
            // 
            // SortearButton
            // 
            this.SortearButton.BackColor = System.Drawing.Color.Goldenrod;
            this.SortearButton.FlatAppearance.BorderColor = System.Drawing.Color.GreenYellow;
            this.SortearButton.FlatAppearance.BorderSize = 0;
            this.SortearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod;
            this.SortearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SortearButton.Font = new System.Drawing.Font("Bahnschrift", 13F);
            this.SortearButton.ForeColor = System.Drawing.Color.Black;
            this.SortearButton.Location = new System.Drawing.Point(169, 427);
            this.SortearButton.Name = "SortearButton";
            this.SortearButton.Size = new System.Drawing.Size(141, 49);
            this.SortearButton.TabIndex = 12;
            this.SortearButton.Text = "Jugar";
            this.SortearButton.UseVisualStyleBackColor = false;
            this.SortearButton.Click += new System.EventHandler(this.SortearButton_Click);
            // 
            // panelJuego
            // 
            this.panelJuego.BackColor = System.Drawing.Color.White;
            this.panelJuego.Controls.Add(this.tableLayoutPanel1);
            this.panelJuego.Controls.Add(this.btnRestar);
            this.panelJuego.Controls.Add(this.TxtPtos3);
            this.panelJuego.Controls.Add(this.TxtPtos6);
            this.panelJuego.Controls.Add(this.TxtPtos9);
            this.panelJuego.Controls.Add(this.TxtPtos8);
            this.panelJuego.Controls.Add(this.TxtPtos5);
            this.panelJuego.Controls.Add(this.TxtPtos2);
            this.panelJuego.Controls.Add(this.TxtPtos0);
            this.panelJuego.Controls.Add(this.TxtPtos7);
            this.panelJuego.Controls.Add(this.TxtPtos4);
            this.panelJuego.Controls.Add(this.TxtPtos1);
            this.panelJuego.Controls.Add(this.LabelMultiplicador);
            this.panelJuego.Controls.Add(this.numericUpDown1);
            this.panelJuego.Controls.Add(this.label2);
            this.panelJuego.Controls.Add(this.Sietebutton);
            this.panelJuego.Controls.Add(this.SortearButton);
            this.panelJuego.Controls.Add(this.Cerobutton);
            this.panelJuego.Controls.Add(this.Nuevebutton);
            this.panelJuego.Controls.Add(this.Unobutton);
            this.panelJuego.Controls.Add(this.Ochobutton);
            this.panelJuego.Controls.Add(this.Dosbutton);
            this.panelJuego.Controls.Add(this.Tresbutton);
            this.panelJuego.Controls.Add(this.Seisbutton);
            this.panelJuego.Controls.Add(this.Cuatrobutton);
            this.panelJuego.Controls.Add(this.Cincobutton);
            this.panelJuego.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelJuego.Location = new System.Drawing.Point(226, 55);
            this.panelJuego.Name = "panelJuego";
            this.panelJuego.Size = new System.Drawing.Size(885, 523);
            this.panelJuego.TabIndex = 13;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.LabelNroGanador, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblBalance, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl4, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(501, 258);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(144, 121);
            this.tableLayoutPanel1.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 30);
            this.label3.TabIndex = 39;
            this.label3.Text = "Numero Ganador";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelNroGanador
            // 
            this.LabelNroGanador.AutoSize = true;
            this.LabelNroGanador.BackColor = System.Drawing.Color.Silver;
            this.LabelNroGanador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelNroGanador.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNroGanador.ForeColor = System.Drawing.Color.Black;
            this.LabelNroGanador.Location = new System.Drawing.Point(0, 30);
            this.LabelNroGanador.Margin = new System.Windows.Forms.Padding(0);
            this.LabelNroGanador.Name = "LabelNroGanador";
            this.LabelNroGanador.Size = new System.Drawing.Size(144, 30);
            this.LabelNroGanador.TabIndex = 18;
            this.LabelNroGanador.Text = "0";
            this.LabelNroGanador.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.BackColor = System.Drawing.Color.Silver;
            this.lblBalance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBalance.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.ForeColor = System.Drawing.Color.Black;
            this.lblBalance.Location = new System.Drawing.Point(0, 90);
            this.lblBalance.Margin = new System.Windows.Forms.Padding(0);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(144, 31);
            this.lblBalance.TabIndex = 42;
            this.lblBalance.Text = "0";
            this.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.lbl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl4.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.ForeColor = System.Drawing.Color.White;
            this.lbl4.Location = new System.Drawing.Point(0, 60);
            this.lbl4.Margin = new System.Windows.Forms.Padding(0);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(144, 30);
            this.lbl4.TabIndex = 41;
            this.lbl4.Text = "Balance";
            this.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRestar
            // 
            this.btnRestar.BackColor = System.Drawing.Color.LightGray;
            this.btnRestar.FlatAppearance.BorderSize = 0;
            this.btnRestar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestar.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestar.Location = new System.Drawing.Point(383, 287);
            this.btnRestar.Margin = new System.Windows.Forms.Padding(2);
            this.btnRestar.Name = "btnRestar";
            this.btnRestar.Size = new System.Drawing.Size(64, 49);
            this.btnRestar.TabIndex = 43;
            this.btnRestar.Text = "Restar";
            this.btnRestar.UseVisualStyleBackColor = false;
            this.btnRestar.Click += new System.EventHandler(this.btnRestar_Click);
            // 
            // TxtPtos3
            // 
            this.TxtPtos3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos3.Location = new System.Drawing.Point(184, 338);
            this.TxtPtos3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos3.Name = "TxtPtos3";
            this.TxtPtos3.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos3.TabIndex = 38;
            this.TxtPtos3.Text = "0";
            this.TxtPtos3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos6
            // 
            this.TxtPtos6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos6.Location = new System.Drawing.Point(255, 341);
            this.TxtPtos6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos6.Name = "TxtPtos6";
            this.TxtPtos6.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos6.TabIndex = 35;
            this.TxtPtos6.Text = "0";
            this.TxtPtos6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos9
            // 
            this.TxtPtos9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos9.Location = new System.Drawing.Point(328, 341);
            this.TxtPtos9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos9.Name = "TxtPtos9";
            this.TxtPtos9.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos9.TabIndex = 36;
            this.TxtPtos9.Text = "0";
            this.TxtPtos9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos8
            // 
            this.TxtPtos8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos8.Location = new System.Drawing.Point(327, 279);
            this.TxtPtos8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos8.Name = "TxtPtos8";
            this.TxtPtos8.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos8.TabIndex = 35;
            this.TxtPtos8.Text = "0";
            this.TxtPtos8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos5
            // 
            this.TxtPtos5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos5.Location = new System.Drawing.Point(255, 280);
            this.TxtPtos5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos5.Name = "TxtPtos5";
            this.TxtPtos5.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos5.TabIndex = 36;
            this.TxtPtos5.Text = "0";
            this.TxtPtos5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos2
            // 
            this.TxtPtos2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos2.Location = new System.Drawing.Point(184, 280);
            this.TxtPtos2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos2.Name = "TxtPtos2";
            this.TxtPtos2.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos2.TabIndex = 37;
            this.TxtPtos2.Text = "0";
            this.TxtPtos2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos0
            // 
            this.TxtPtos0.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos0.Location = new System.Drawing.Point(110, 280);
            this.TxtPtos0.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos0.Name = "TxtPtos0";
            this.TxtPtos0.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos0.TabIndex = 35;
            this.TxtPtos0.Text = "0";
            this.TxtPtos0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos7
            // 
            this.TxtPtos7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos7.Location = new System.Drawing.Point(327, 219);
            this.TxtPtos7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos7.Name = "TxtPtos7";
            this.TxtPtos7.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos7.TabIndex = 36;
            this.TxtPtos7.Text = "0";
            this.TxtPtos7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos4
            // 
            this.TxtPtos4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos4.Location = new System.Drawing.Point(255, 219);
            this.TxtPtos4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos4.Name = "TxtPtos4";
            this.TxtPtos4.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos4.TabIndex = 37;
            this.TxtPtos4.Text = "0";
            this.TxtPtos4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtPtos1
            // 
            this.TxtPtos1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtos1.Location = new System.Drawing.Point(183, 219);
            this.TxtPtos1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TxtPtos1.Name = "TxtPtos1";
            this.TxtPtos1.Size = new System.Drawing.Size(17, 15);
            this.TxtPtos1.TabIndex = 34;
            this.TxtPtos1.Text = "0";
            this.TxtPtos1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelMultiplicador
            // 
            this.LabelMultiplicador.AutoSize = true;
            this.LabelMultiplicador.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMultiplicador.Location = new System.Drawing.Point(530, 395);
            this.LabelMultiplicador.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelMultiplicador.Name = "LabelMultiplicador";
            this.LabelMultiplicador.Size = new System.Drawing.Size(78, 14);
            this.LabelMultiplicador.TabIndex = 33;
            this.LabelMultiplicador.Text = "Multiplicador:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.Color.Silver;
            this.numericUpDown1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericUpDown1.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown1.ForeColor = System.Drawing.Color.Black;
            this.numericUpDown1.Location = new System.Drawing.Point(533, 427);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(75, 18);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 21;
            // 
            // SaldoActualLabel
            // 
            this.SaldoActualLabel.AutoSize = true;
            this.SaldoActualLabel.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaldoActualLabel.ForeColor = System.Drawing.Color.White;
            this.SaldoActualLabel.Location = new System.Drawing.Point(137, 40);
            this.SaldoActualLabel.Name = "SaldoActualLabel";
            this.SaldoActualLabel.Size = new System.Drawing.Size(16, 18);
            this.SaldoActualLabel.TabIndex = 17;
            this.SaldoActualLabel.Text = "0";
            // 
            // buttonCargar
            // 
            this.buttonCargar.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonCargar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCargar.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCargar.ForeColor = System.Drawing.Color.White;
            this.buttonCargar.Location = new System.Drawing.Point(70, 206);
            this.buttonCargar.Name = "buttonCargar";
            this.buttonCargar.Size = new System.Drawing.Size(75, 38);
            this.buttonCargar.TabIndex = 16;
            this.buttonCargar.Text = "Cargar";
            this.buttonCargar.UseVisualStyleBackColor = false;
            this.buttonCargar.Click += new System.EventHandler(this.ButtonCargar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(39, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 18);
            this.label1.TabIndex = 15;
            this.label1.Text = "Carga tu saldo aquí:";
            // 
            // textCargaSaldo
            // 
            this.textCargaSaldo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCargaSaldo.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCargaSaldo.Location = new System.Drawing.Point(42, 161);
            this.textCargaSaldo.Name = "textCargaSaldo";
            this.textCargaSaldo.Size = new System.Drawing.Size(133, 19);
            this.textCargaSaldo.TabIndex = 14;
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.FlatAppearance.BorderColor = System.Drawing.Color.OrangeRed;
            this.btnCerrarSesion.FlatAppearance.BorderSize = 2;
            this.btnCerrarSesion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.btnCerrarSesion.Location = new System.Drawing.Point(54, 481);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(109, 30);
            this.btnCerrarSesion.TabIndex = 40;
            this.btnCerrarSesion.Text = "Cerrar Sesión";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // lblBienvenido
            // 
            this.lblBienvenido.AutoSize = true;
            this.lblBienvenido.BackColor = System.Drawing.Color.Goldenrod;
            this.lblBienvenido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBienvenido.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienvenido.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.lblBienvenido.Location = new System.Drawing.Point(882, 14);
            this.lblBienvenido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBienvenido.Name = "lblBienvenido";
            this.lblBienvenido.Size = new System.Drawing.Size(19, 26);
            this.lblBienvenido.TabIndex = 41;
            this.lblBienvenido.Text = "-";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(46)))), ((int)(((byte)(59)))));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnCerrarSesion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 523);
            this.panel1.TabIndex = 44;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.buttonCargar);
            this.panel2.Controls.Add(this.textCargaSaldo);
            this.panel2.Controls.Add(this.SaldoActualLabel);
            this.panel2.Location = new System.Drawing.Point(0, 97);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(228, 312);
            this.panel2.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(47, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 18);
            this.label4.TabIndex = 18;
            this.label4.Text = "Saldo Actual:";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1111, 578);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblBienvenido);
            this.Controls.Add(this.panelJuego);
            this.Controls.Add(this.menuStrip2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormPrincipal";
            this.Text = "FormPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panelJuego.ResumeLayout(false);
            this.panelJuego.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem listadoDeDatosDeUsuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarUsuarioToolStripMenuItem;
        private System.Windows.Forms.Button Cerobutton;
        private System.Windows.Forms.Button Unobutton;
        private System.Windows.Forms.Button Dosbutton;
        private System.Windows.Forms.Button Tresbutton;
        private System.Windows.Forms.Button Cuatrobutton;
        private System.Windows.Forms.Button Cincobutton;
        private System.Windows.Forms.Button Seisbutton;
        private System.Windows.Forms.Button Sietebutton;
        private System.Windows.Forms.Button Ochobutton;
        private System.Windows.Forms.Button Nuevebutton;
        private System.Windows.Forms.Button SortearButton;
        private System.Windows.Forms.Panel panelJuego;
        private System.Windows.Forms.TextBox textCargaSaldo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCargar;
        private System.Windows.Forms.Label SaldoActualLabel;
        private System.Windows.Forms.Label LabelNroGanador;
        private System.Windows.Forms.NumericUpDown numericUpDown1;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem listadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jugadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ListadoMovimientosStrip;
        private System.Windows.Forms.Label TxtPtos3;
        private System.Windows.Forms.Label TxtPtos6;
        private System.Windows.Forms.Label TxtPtos9;
        private System.Windows.Forms.Label TxtPtos8;
        private System.Windows.Forms.Label TxtPtos5;
        private System.Windows.Forms.Label TxtPtos2;
        private System.Windows.Forms.Label TxtPtos0;
        private System.Windows.Forms.Label TxtPtos7;
        private System.Windows.Forms.Label TxtPtos4;
        private System.Windows.Forms.Label TxtPtos1;
        private System.Windows.Forms.Label LabelMultiplicador;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Label lblBienvenido;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Button btnRestar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}